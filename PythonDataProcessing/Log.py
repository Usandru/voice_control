# -*- coding: utf-8 -*-
"""
Created on Sun May 27 13:47:11 2018

@author: rargo
"""

from collections import Counter
import numpy as np

class TestLog:
    
    def __init__(self, log_path):
        
        fixed_log_items = ["item collected", "level complete", "switch activated", "reset", "time at start", "go-to object not found"]
        
        self.valid_inputs = set(["step up", "step left", "step right", "step down", "go to", "move to", "roll to", "move up", "move left", "move right", "move down",
                             "yes", "confirm", "affirm", "affirmative", "correct", "no", "negative", "wrong", "deny", "cancel", "stop", "go back", "undo",
                             "instruction start", "start", "begin", "go", "instruction end", "end", "execute", "activate", "1", "2", "3", "4", "5", "6", "7", "8",
                             "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"])
        
        self.id = log_path.strip(".txt")
        
        self.voice_time = []
        self.voice_content = []
        self.voice_valid_content = set()
        self.voice_invalid_content = set()
        self.voice_goto_calls = 0
        self.voice_goto_errors = 0
        self.voice_offset = None
        self.voice_completion_time = None
        self.voice_successful = True
        
        self.keyboard_time = []
        self.keyboard_content = []
        self.keyboard_valid_content = set()
        self.keyboard_invalid_content = set()
        self.keyboard_invalid_content_counter = 0
        self.keyboard_goto_calls = 0
        self.keyboard_goto_errors = 0
        self.keyboard_offset = None
        self.keyboard_completion_time = None
        self.keyboard_successful = True
        
        self.input_order = None
        self.level_order = None
        
        current_log_is_voice = False
        
        log = open(log_path, encoding="utf-8")
        
        log.readline()
        
        for lines in log:
            line = lines.strip("\n")
            
            if line == "Level_1_Final" or line == "Level_2_Final":
                if self.level_order == None:
                    if line == "Level_1_Final":
                        self.level_order = 1
                    else:
                        self.level_order = 2
            elif line == "Loading Keyboard Level" or line == "Loading Voice Level":
                if self.input_order == None:
                    if line == "Loading Keyboard Level":
                        self.input_order = 1
                    else:
                        self.input_order = 2
                        current_log_is_voice = True
                else:
                    current_log_is_voice = not current_log_is_voice
            elif line == "LEVEL LOAD OVERRIDE":
                if current_log_is_voice:
                    self.voice_successful = False
                else:
                    self.keyboard_successful = False
            else:
                line_seg = line.split("; ")
                
                if line_seg[1] in fixed_log_items:
                    if line_seg[1] == "go-to object not found":
                        if current_log_is_voice:
                            self.voice_goto_errors += 1
                        else:
                            self.keyboard_goto_errors += 1
                    continue

                
                if current_log_is_voice:
                    if line_seg[1] in self.valid_inputs:
                        self.voice_valid_content.add(line_seg[1])
                        if line_seg[1] == "go to" or line_seg[1] == "move to" or line_seg[1] == "roll to":
                            self.voice_goto_calls += 1
                    else:
                        self.voice_invalid_content.add(line_seg[1])
                    self.voice_time.append(float(line_seg[0]))
                    self.voice_content.append("_".join(line_seg[1:]))
                    
                else:
                    if line_seg[1] in self.valid_inputs:
                        self.keyboard_valid_content.add(line_seg[1])
                        if line_seg[1] == "go to" or line_seg[1] == "move to" or line_seg[1] == "roll to":
                            self.keyboard_goto_calls += 1
                    else:
                        self.keyboard_invalid_content.add(line_seg[1])
                        self.keyboard_invalid_content_counter += 1
                        
                    self.keyboard_time.append(float(line_seg[0]))
                    self.keyboard_content.append(line_seg[1])
                
        log.close()
        
        self.voice_completion_time = self.voice_time[-1] - self.voice_time[1]
        self.keyboard_completion_time = self.keyboard_time[-1] - self.keyboard_time[1]
        
        self.voice_avg_time = np.diff(self.voice_time)
        self.keyboard_avg_time = np.diff(self.keyboard_time)
        
        self.voice_offset = self.voice_time[0]
        self.voice_time[:] = [x - self.voice_offset for x in self.voice_time]
        
        self.keyboard_offset = self.keyboard_time[0]
        self.keyboard_time[:] = [x - self.keyboard_offset for x in self.keyboard_time]
        
    
    def voice_get_most_common(self, number_to_get):
        voice_counter = Counter(self.voice_content)
        
        return voice_counter.most_common(number_to_get)
                
    def keyboard_get_most_common(self, number_to_get):
        keyboard_counter = Counter(self.keyboard_content)
        
        return keyboard_counter.most_common(number_to_get)
    
    def average_time_between_actions(self):
        voice_avg = sum(self.voice_avg_time) / len(self.voice_avg_time)
        keyboard_avg = sum(self.keyboard_avg_time) / len(self.keyboard_avg_time)
        
        return voice_avg, keyboard_avg
    
    def num_of_errors(self):
        voice_errors = self.voice_goto_errors
        keyboard_errors = self.keyboard_invalid_content_counter - (self.keyboard_goto_calls - self.keyboard_goto_errors)
        
        return voice_errors, keyboard_errors
    
    def action_spread(self):
        return len(self.voice_valid_content), len(self.keyboard_valid_content)
    
    def validate(self):
        check_file_path = "VALIDATE_" + self.id + ".txt"
        
        validation_file = open(check_file_path, "w", encoding="utf-8")
        
        validation_file.write("junkline")
        
        if self.level_order == 1:
            validation_file.write("\nLevel_1_Final")
        else:
            validation_file.write("\nLevel_2_Final")
        
        if self.input_order == 1:
            validation_file.write("\nLoading Keyboard Level")
            for i in range(len(self.keyboard_time)):
                validation_file.write("\n" + "; ".join([str(self.keyboard_time[i]), self.keyboard_content[i]]))
            if not self.keyboard_successful:
                validation_file.write("\nLEVEL LOAD OVERRIDE")
                
        else:
            validation_file.write("\nLoading Voice Level")
            for i in range(len(self.voice_time)):
                content = self.voice_content[i].split("_")
                
                parts = [str(self.voice_time[i])] + content
                
                validation_file.write("\n" + "; ".join(parts))
            if not self.voice_successful:
                validation_file.write("\nLEVEL LOAD OVERRIDE")
                
        
        
        ###Do the same thing, but using the reverse paths down the if
        if self.level_order == 2:
            validation_file.write("\nLevel_1_Final")
        else:
            validation_file.write("\nLevel_2_Final")
        
        if self.input_order == 2:
            validation_file.write("\nLoading Keyboard Level")
            for i in range(len(self.keyboard_time)):
                validation_file.write("\n" + "; ".join([str(self.keyboard_time[i]), self.keyboard_content[i]]))
            if not self.keyboard_successful:
                validation_file.write("\nLEVEL LOAD OVERRIDE")
                
        else:
            validation_file.write("\nLoading Voice Level")
            for i in range(len(self.voice_time)):
                content = self.voice_content[i].split("_")
                
                parts = [str(self.voice_time[i])] + content
                
                validation_file.write("\n" + "; ".join(parts))
            if not self.voice_successful:
                validation_file.write("\nLEVEL LOAD OVERRIDE")
                
        validation_file.close()
        
        validation_file = open(check_file_path, encoding="utf-8")
        original_file = open(self.id + ".txt", encoding="utf-8")
        
        validation_file.readline()
        original_file.readline()
        
        i = 0
        for line in original_file:
            if not validation_file.readline() == line:
                print(self.id)
                print(line)
                print("LINE MISMATCH AT: " + str(i))
            i += 1
            
        validation_file.close()
        original_file.close()