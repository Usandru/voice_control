# -*- coding: utf-8 -*-
"""
Created on Tue May 29 21:37:45 2018

@author: Andreas
"""

import plotly.offline as offline
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go
from Log import TestLog

config = open("config.txt", encoding="utf-8")

logs = []

for line in config:
    path = line.strip("\n")
    
    logs.append(TestLog(path))
    
config.close()

voice_plot_completed = []
voice_plot_failed = []

kb_plot_completed = []
kb_plot_failed = []

log_diff_time = []
log_voice_avg_time = []
log_keyboard_errors = []
log_id = []

output_csv = open("output.csv", mode="w", encoding="utf-8")

output_csv.write("ID;average time between actions in seconds (voice);average time between actions in seconds (keyboard);number of unique actions (voice);number of unique actions (keyboard);number of goto errors (voice);number of input errors (keyboard);input ordering (1 = keyboard, 2 = voice);level ordering;voice level finished;keyboard level finished;voice completion time in seconds;keyboard completion time in seconds;most common voice input;most common keyboard input\n")

for log in logs:
    #log.validate() ### currently broken, likely not gonna fix
    
    k_most_c = log.keyboard_get_most_common(None)
    v_most_c = log.voice_get_most_common(None)
        
    vi, ki = log.average_time_between_actions()
    
    
        
    v_spread, k_spread = log.action_spread()
    
    v_errors, k_errors = log.num_of_errors()
    
    v_total = len(log.voice_time)
    k_total = len(log.keyboard_time)
    
    vx_len = np.linspace(0, v_total, v_total)
    kx_len = np.linspace(0, k_total, k_total)
    
    #voice_plot.append(go.Scatter(x = vx_len, y = log.voice_time, mode="lines+markers", name=log.id))
    #voice_avg_plot.append(go.Scatter(x = vx_len, y = log.voice_avg_time, mode="lines+markers", name=log.id))
    
    #kb_plot.append(go.Scatter(x = kx_len, y = log.keyboard_time, mode="lines+markers", name=log.id))
    #kb_avg_plot.append(go.Scatter(x = kx_len, y = log.keyboard_avg_time, mode="lines+markers", name=log.id))
    
    if log.keyboard_successful:
        kb_plot_completed.append(go.Scatter(x = kx_len, y = log.keyboard_time, mode="lines+markers", name=log.id))
        
        if log.voice_successful:
            voice_plot_completed.append(go.Scatter(x = vx_len, y = log.voice_time, mode="lines+markers", name=log.id))
            
            log_diff_time.append(log.voice_completion_time - log.keyboard_completion_time)
            log_voice_avg_time.append(vi)
            log_keyboard_errors.append(k_errors)
            log_id.append(log.id)
        else:
            voice_plot_failed.append(go.Scatter(x = vx_len, y = log.voice_time, name=log.id, line = dict(dash = 'dash')))
            
    elif log.voice_successful:
        kb_plot_failed.append(go.Scatter(x = kx_len, y = log.keyboard_time, name=log.id, line = dict(dash = 'dash')))
        voice_plot_completed.append(go.Scatter(x = vx_len, y = log.voice_time, mode="lines+markers", name=log.id))
        
    else:
        voice_plot_failed.append(go.Scatter(x = vx_len, y = log.voice_time, name=log.id, line = dict(dash = 'dash')))
        kb_plot_failed.append(go.Scatter(x = kx_len, y = log.keyboard_time, name=log.id, line = dict(dash = 'dash')))
    
    output = ";".join([str(log.id), str(vi), str(ki), str(v_spread), str(k_spread), 
                       str(v_errors), str(k_errors), str(log.input_order), str(log.level_order),
                       str(log.voice_successful), str(log.keyboard_successful), 
                       str(log.voice_completion_time), str(log.keyboard_completion_time), str(v_most_c), str(k_most_c)])
    
    output_csv.write(output + "\n")
    
output_csv.close()

voice_plot_combined = voice_plot_completed + voice_plot_failed
keyboard_plot_combined = kb_plot_completed + kb_plot_failed

fig1_layout=go.Layout(title="Combined Voice Data", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig1 = go.Figure(data=voice_plot_combined, layout=fig1_layout)
offline.plot(fig1, filename='v_plot_combine.html', image_filename='voice_plot', image='png')

fig2_layout=go.Layout(title="Combined Keyboard Data", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig2 = go.Figure(data=keyboard_plot_combined, layout=fig2_layout)
offline.plot(fig2, filename='k_plot_combine.html', image_filename='keyboard_plot', image='png')

fig3_layout=go.Layout(title="Completed Voice Levels", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig3 = go.Figure(data=voice_plot_completed, layout=fig3_layout)
offline.plot(fig3, filename='v_plot_complete.html', image_filename='voice_plot_c', image='png')

fig4_layout=go.Layout(title="Completed Keyboard Levels", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig4 = go.Figure(data=kb_plot_completed, layout=fig4_layout)
offline.plot(fig4, filename='k_plot_complete.html', image_filename='keyboard_plot_c', image='png')

fig5_layout=go.Layout(title="Failed Voice Data", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig5 = go.Figure(data=voice_plot_failed, layout=fig5_layout)
offline.plot(fig5, filename='v_plot_failed.html', image_filename='voice_plot_f', image='png')

fig6_layout=go.Layout(title="Failed Keyboard Data", xaxis={'title':'input actions'}, yaxis={'title':'time in seconds'})
fig6 = go.Figure(data=kb_plot_failed, layout=fig6_layout)
offline.plot(fig6, filename='k_plot_failed.html', image_filename='keyboard_plot_f', image='png')

fig7_layout=go.Layout(title="Distribution of difference in completion time against voice action input speed", xaxis={'title':'difference in completion time = voice level completion time - keyboard level completion time'}, yaxis={'title':'average interval between inputs during voice level in seconds'})
time_performance_scatter = go.Scatter(x = log_diff_time, y = log_voice_avg_time, mode="markers", marker=dict(size = 25), name="scatterplot")
data_1 = [time_performance_scatter]
fig7 = go.Figure(data=data_1, layout=fig7_layout)
offline.plot(fig7, filename="voice_scatter", image_filename='voice_scatter', image='png')

fig8_layout=go.Layout(title="Distribution of difference in completion time against keyboard input errors", xaxis={'title':'difference in completion time = voice level completion time - keyboard level completion time'}, yaxis={'title':'number of invalid keyboard inputs'})
error_perform_scatter = go.Scatter(x = log_diff_time, y = log_keyboard_errors, mode='markers', marker=dict(size = 30), name="scatterplot")
data_2 = [error_perform_scatter]
fig8 = go.Figure(data=data_2, layout=fig8_layout)
offline.plot(fig8, filename="kb_error_scatter", image_filename="error_kb_scatter", image='png')