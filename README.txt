To run this project, get Unity version: 2017.4.1f1
Then clone the project into Unity.
To run the game, switch to the scene Main. Once started, press any key to start the game.

To run the analytics, go into PythonDataProcessing and run the Main.py script.