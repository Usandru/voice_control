﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyInput : InputControllerABC {

    List<string> DummyCommandList;
    // Use this for initialization
    void Start () {
        DummyCommandList = new List<string>{ "move up", "move up", "move up", "move up", "move up", "move up", "move up" };
        DummyCommandList.AddRange(new List<string>{ "move down", "move down", "move down", "move down" });
        DummyCommandList.AddRange(new List<string> { "move right", "move right", "move left"});

        StartCoroutine(LateStart(1));
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        //Your Function You Want to Call
        ExecutionController.instance.BeginExecution(DummyCommandList);
    }

    public override void ResumeInput()
    {
        //do nothing
        Debug.Log("finished execution");
    }
}