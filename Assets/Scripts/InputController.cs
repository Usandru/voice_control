﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using System.Collections.Generic;

public class InputController: InputControllerABC {
	
    [SerializeField]
	InputField inputField;
	[SerializeField]
	InstructionListController instruction_list;
	ExecutionController executor;

    List<string> start_execution_commands = new List<string> { "instruction end", "end", "execute", "activate", "begin" };
    List<string> cancel_commands = new List<string> { "cancel", "stop", "go back" };
    List<string> undo = new List<string> { "undo" };
    List<string> reset = new List<string> { "reset" };

    bool receiving_commands = true;
    bool receiving_numeric = false;
    bool receiving_context = false;
    bool input_active = true;

    string current_command;

	private string userInput;
	private string context;

    private void Start()
    {
        executor = ExecutionController.instance;
        inputField.ActivateInputField();
        InstructionTextController.instance.SetText("");
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; time at start");
    }

    public void OnSubmit()
		{
        if (reset.Contains(inputField.text))
        {
            input_active = true;
            executor.Reset();
        }

        if (!input_active) return;

        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + inputField.text);

        Debug.Log("You said: " + inputField.text);

        if (inputField.text.Length == 0) return;

        if(start_execution_commands.Contains(inputField.text))
        {
            instruction_list.Execute_Commands();
            input_active = false;
        }

        if (receiving_commands)
        {
            if (executor.GetCommands().Contains(inputField.text))
            {
                instruction_list.Add_Command(inputField.text);
            } else if (executor.GetContextCommands().Contains(inputField.text))
            {
                current_command = inputField.text;
                receiving_commands = false;
                receiving_context = true;
                InstructionTextController.instance.SetText("Input the name of the object to go to.");
            } else if (executor.GetNumericCommands().Contains(inputField.text))
            {
                current_command = inputField.text;
                receiving_commands = false;
                receiving_numeric = true;
                InstructionTextController.instance.SetText("Input the number of steps to take.");
            } else if (undo.Contains(inputField.text))
            {
                instruction_list.Undo();
            }
        } else if (receiving_context)
        {
            if (cancel_commands.Contains(inputField.text))
            {
                InstructionTextController.instance.SetText("");
                receiving_context = false;
                return;
            }
            instruction_list.Add_Context_Command(current_command, inputField.text);
            receiving_commands = true;
            receiving_context = false;
            InstructionTextController.instance.SetText("");
        } else if (receiving_numeric)
        {
            if (cancel_commands.Contains(inputField.text))
            {
                InstructionTextController.instance.SetText("");
                receiving_numeric = false;
                return;
            }
            int numeric;
            if (int.TryParse(inputField.text, out numeric))
            {
                instruction_list.Add_Context_Command(current_command, numeric.ToString());
                receiving_commands = true;
                receiving_numeric = false;
                InstructionTextController.instance.SetText("");
            }
        }
        else
        {
            //input not valid
        }

        inputField.text = "";
        inputField.ActivateInputField();

	}

    public override void ResumeInput()
    {
        input_active = true;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            OnSubmit();
        }
    }

}




