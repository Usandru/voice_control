﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    [SerializeField]
    float open_pos_x;
    [SerializeField]
    float open_pos_y;
    [SerializeField]
    float open_pos_z;
    [SerializeField]
    float speed;
    [SerializeField]
    Transform self_transform;

    Vector3 open;
    Vector3 close;
    bool flipped = false;
    bool is_open = false;

    private void Start()
    {
        close = self_transform.position;
        open = new Vector3(self_transform.position.x + open_pos_x, self_transform.position.y + open_pos_y, self_transform.position.z + open_pos_z);
    }

    public void flip () {
        flipped = true;
	}

    private void Update()
    {
        if (flipped)
        {
            if (!is_open) {
                self_transform.position = Vector3.MoveTowards(self_transform.position, open, Time.deltaTime*speed);
                if (self_transform.position == open)
                {
                    flipped = false;
                    is_open = true;
                }
            }
            else
            {
                self_transform.position = Vector3.MoveTowards(self_transform.position, close, Time.deltaTime*speed);
                if (self_transform.position == close)
                {
                    flipped = false;
                    is_open = false;
                }
            }
            

        }
    }

}
