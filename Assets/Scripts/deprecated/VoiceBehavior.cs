﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System;
using System.Text;
using System.Linq;


public class VoiceBehavior : MonoBehaviour {
	public MovementSystem movement;
	public Rigidbody rb;

	private Dictionary<string, Action> keywordActions = new Dictionary<string,Action>();
	private KeywordRecognizer keywordRecognizer;



	// Use this for initialization
	void Start () 
	{

		keywordActions.Add ("go",Go);
		//keywordActions.Add("stop",movement.Stop());
		keywordActions.Add("go left",Left);
		//keywordActions.Add("go right",movement.Right());
		//keywordActions.Add("go up",movement.Up());
		//keywordActions.Add("go down", movement.Down());

		keywordRecognizer = new KeywordRecognizer (keywordActions.Keys.ToArray());
		keywordRecognizer.OnPhraseRecognized += OnKeywordsRecognized;
		keywordRecognizer.Start();


		

	}

    void Go()
    {
        movement.Go();
    }

    void Left()
    {
        movement.Left();
    }

	private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
	{
		Debug.Log("Keyword: " + args.text);
		keywordActions[args.text].Invoke();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
