﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementState { OK, FAILED, BLOCKED}

public class MovementSystem : MonoBehaviour {

    float speed = 0f;
    float horizontal = 0f;
    float vertical = 0f;
    [SerializeField]
    Rigidbody rb_self;


    public MovementState Go()
    {
        speed = 1;
        print("Go called");
        return MovementState.OK;
    }

    public MovementState Stop()
    {
        speed = 0;
        print("Stop called");
        return MovementState.OK;
    }

    public MovementState Left()
    {
        horizontal = 1f;
        vertical = 0f;
        print("Left called");
        return MovementState.OK;
    }

    public MovementState Right()
    {
        horizontal = -1f;
        vertical = 0f;
        print("Right called");
        return MovementState.OK;
    }

    public MovementState Up()
    {
        vertical = 1f;
        horizontal = 0f;
        print("Up called");
        return MovementState.OK;
    }

    public MovementState Down()
    {
        vertical = -1f;
        horizontal = 0f;
        print("Down called");
        return MovementState.OK;
    }

    private void Update()
    {
        rb_self.AddForce(speed * horizontal, 0, speed * vertical);
    }

}
