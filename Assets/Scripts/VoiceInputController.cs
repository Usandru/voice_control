﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Text;
using System.Linq;

public class VoiceInputController : InputControllerABC {

    public static VoiceInputController instance;

    //Special keywords - if there's complex behaviour in OnKeywordsRecognized that requires matching these, make a string-var for the keyword
    //so the actual keyword can be easily changed. General command keywords should be added in ExecutionController.

    //the listener for commands. It is the top-level listener and all initial commands should be added to it
    private KeywordRecognizer command_listener;
    private DictationRecognizer context_listener;
    private KeywordRecognizer numeric_listener;
    private KeywordRecognizer control_listener;
    private KeywordRecognizer cancel_listener;

    //State and control variables
    bool receivingInstructions = false;
    string current_command = null;
    string current_param = null;
    string[] keywords;
    string[] control;
    string[] numerics = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" };
    string[] affirm = { "yes", "confirm", "affirm", "affirmative", "correct" };
    string[] deny = { "no", "negative", "wrong", "deny" };
    string[] cancel = { "cancel", "stop", "go back" };
    string[] undo = { "undo" };
    string[] begin_commands = { "instruction start", "start", "begin", "go" };
    string[] end_commands = { "instruction end", "end", "execute", "activate" };
    
    [SerializeField]
    InstructionListController instruction_list;
    [SerializeField]
    ExecutionController executor;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        //remember to add new keywords to the string array
        List<string> keyword_list = new List<string>();
        keyword_list.AddRange(executor.GetCommands());
        keyword_list.AddRange(executor.GetContextCommands());
        keyword_list.AddRange(executor.GetNumericCommands());
        keyword_list.AddRange(begin_commands);
        keyword_list.AddRange(end_commands);
        keyword_list.AddRange(undo);

        List<string> control_list = new List<string>();
        control_list.AddRange(affirm);
        control_list.AddRange(deny);

        keywords = keyword_list.ToArray();

        control = control_list.ToArray();

        Debug.Log(string.Join(" ", keywords));

        StartCommandRecognition();
        InstructionTextController.instance.SetText("To begin, please say 'instruction start'");
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; time at start");
    }
	
    private void StartCommandRecognition()
    {
        //Debug.Log(PhraseRecognitionSystem.Status);
        if (cancel_listener != null) cancel_listener.Dispose();
        command_listener = new KeywordRecognizer(keywords);
        command_listener.OnPhraseRecognized += OnKeywordsRecognized;
        command_listener.Start();
    }

    private void StartNumericRecognition()
    {
        numeric_listener = new KeywordRecognizer(numerics);
        numeric_listener.OnPhraseRecognized += OnNumericRecognized;
        numeric_listener.Start();
        StartCancelRecognition();
    }

    private void StartControlRecognition()
    {
        control_listener = new KeywordRecognizer(control);
        control_listener.OnPhraseRecognized += OnControlRecognized;
        control_listener.Start();
        StartCancelRecognition();
    }

    private void StartContextRecognition()
    {
        context_listener = new DictationRecognizer();
        context_listener.DictationResult += OnDictationInput;
        context_listener.DictationComplete += OnDictationTimeout;
        context_listener.Start();
    }

    private void StartCancelRecognition()
    {
        cancel_listener = new KeywordRecognizer(cancel);
        cancel_listener.OnPhraseRecognized += OnCancel;
        cancel_listener.Start();
    }

    private void OnCancel(PhraseRecognizedEventArgs args)
    {
        ResetToCommand();
    }

    private void OnKeywordsRecognized(PhraseRecognizedEventArgs args)
    {
        Debug.Log(args.text);
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + args.text);

        if (receivingInstructions)
        {
            if(end_commands.Contains<string>(args.text))
            {
                //Debug.Log("passing commands to executor");
                //pass control to the next stage and turn off the listener temporarily.
                command_listener.Dispose();
                if (numeric_listener != null) numeric_listener.Dispose();
                instruction_list.Execute_Commands();
                InstructionTextController.instance.SetText("");
            }
            else
            {
                if (executor.GetContextCommands().Contains(args.text))
                {
                    current_command = args.text;
                    command_listener.Dispose();
                    PhraseRecognitionSystem.Shutdown();
                    StartContextRecognition();
                    context_listener.Start();
                    InstructionTextController.instance.SetText("You are giving a 'go to' command. Specify the object you want to go to.");
                }
                else if (executor.GetNumericCommands().Contains(args.text))
                {
                    //get a numeric param
                    current_command = args.text;
                    command_listener.Stop();
                    StartNumericRecognition();
                    InstructionTextController.instance.SetText("You are specifying a number of steps to move. Give a number between 1 and 20.");
                }
                else if (executor.GetCommands().Contains(args.text))
                {
                    //just send the string to the instruction list
                    instruction_list.Add_Command(args.text);
                } else if (undo.Contains<string>(args.text))
                {
                    instruction_list.Undo();
                }
            }
        }
        else
        {
            //check if instructions should be received yet
            if (begin_commands.Contains<string>(args.text))
            {
                receivingInstructions = true;
                InstructionTextController.instance.SetText("You may give commands. Say 'instruction end' to execute.");
            }
        }
    }

    private void OnNumericRecognized(PhraseRecognizedEventArgs args)
    {
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + args.text);
        Debug.Log("numeric recieved: " + args.text);
        instruction_list.Add_Context_Command(current_command, args.text);
        numeric_listener.Dispose();
        cancel_listener.Dispose();
        command_listener.Start();
        InstructionTextController.instance.SetText("You may give commands. Say 'instruction end' to execute.");
    }

    private void OnControlRecognized(PhraseRecognizedEventArgs args)
    {
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + args.text);
        control_listener.Dispose();
        if (affirm.Contains<string>(args.text))
        {
            instruction_list.Add_Context_Command(current_command, InputToLowercase(current_param));
            control_listener.Dispose();
            StartCommandRecognition();
            InstructionTextController.instance.SetText("You may give commands. Say 'instruction end' to execute.");
        }
        else
        {
            PhraseRecognitionSystem.Shutdown();
            Clear();
            StartContextRecognition();
            InstructionTextController.instance.SetText("You are giving a 'go to' command. Specify the object you want to go to.");
        }
    }

    private void OnDictationInput(string text, ConfidenceLevel confidence)
    {
        TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; " + text + "; " + confidence);
        //we'll probably want to make this behaviour a little more advanced.
        Debug.Log("context input: " + text);
        context_listener.Dispose();
        if (confidence == ConfidenceLevel.High)
        {
            instruction_list.Add_Context_Command(current_command, InputToLowercase(text));
            StartCommandRecognition();
            InstructionTextController.instance.SetText("You may give commands. Say 'instruction end' to execute.");
        }
        else
        {
            //this should show text in the middle of the screen or something
            Clear();
            StartControlRecognition();
            current_param = text;
            InstructionTextController.instance.SetText("Did you say \"" + text + "\"? Say 'yes' to confirm, 'no' to specify an object again.");
        }

    }

    private void OnDictationTimeout(DictationCompletionCause cause)
    {
        if (cause == DictationCompletionCause.TimeoutExceeded)
        {
            TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; dictation timeout");
            context_listener.Start();
        }
    }

    public override void ResumeInput()
    {
        StartCommandRecognition();
        InstructionTextController.instance.SetText("You may give commands. Say 'instruction end' to execute.");
    }

    public void Clear()
    {
        if (command_listener != null) command_listener.Dispose();
        if (numeric_listener != null) numeric_listener.Dispose();
        if (control_listener != null) control_listener.Dispose();
        if (cancel_listener != null) cancel_listener.Dispose();
    }

    public void ResetToCommand()
    {
        if (context_listener != null) context_listener.Dispose();
        if (command_listener != null) command_listener.Dispose();
        if (numeric_listener != null) numeric_listener.Dispose();
        if (control_listener != null) control_listener.Dispose();

        StartCommandRecognition();
    }

    string InputToLowercase(string input_text)
    {
        return input_text.ToLower();
    }

}
