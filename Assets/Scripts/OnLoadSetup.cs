﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class OnLoadSetup : MonoBehaviour {

    public static OnLoadSetup instance;

    [SerializeField]
    GameObject voice_controller;
    [SerializeField]
    GameObject keyboard_controller;

    public int pickup_counter = 0;
    bool game_ended = false;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        if (GameManager.instance.voice_level)
        {
            //enable voice input
            voice_controller.SetActive(true);
        }
        else
        {
            //enable keyboard input
            keyboard_controller.SetActive(true);

        }
	}

    public void CheckVictory()
    {
        if(pickup_counter >= 4)
        {
            TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; level complete");
            game_ended = true;
            InstructionTextController.instance.SetText("Press the 'enter' key to end the level.");
        }
    }

    private void Update()
    {
        if (!game_ended) return;

        if (Input.GetButtonDown("Submit"))
        {
            GameManager.instance.LoadLevel();
        }
    }
}
