﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputControllerABC : MonoBehaviour {

    public virtual void ResumeInput()
    {
        //you have to implement a version of ResumeInput for all subclasses of InputControllerABC.
    }

}
