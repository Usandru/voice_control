﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ExecutionController : MonoBehaviour {

    //variables
    public static ExecutionController instance = null;              //Static instance of ExecutionController which allows it to be accessed by any other script.
    Queue<string> instruction_list = new Queue<string>();
    NNConstraint constraint;
    PlayerMover player;
    bool isExecuting = false;
    public bool activeAction = false;
    //No-context command keywords
    List<string> command_list = new List<string> { "step up", "step left", "step right", "step down" };
    //Command keywords requiring freeform context parameters
    List<string> set_of_context_keywords = new List<string> { "go to", "move to", "roll to" };
    //Command keywords requiring a numeric parameter
    List<string> set_of_numeric_keywords = new List<string> { "move up", "move left", "move right", "move down" };
    [SerializeField]
    InputControllerABC input_controller;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

    }

    private void Start()
    {
        GameObject playerObject = GameObject.FindWithTag("Player");
        player = playerObject.GetComponent<PlayerMover>();
    }

    public List<string> GetCommands()
    {
        return command_list;
    }

    public List<string> GetContextCommands()
    {
        return set_of_context_keywords;
    }

    public List<string> GetNumericCommands()
    {
        return set_of_numeric_keywords;
    }

    public void BeginExecution(List<string> list)
    {
        instruction_list = new Queue<string>(list);

        if(instruction_list.Count == 0)
        {
            input_controller.ResumeInput();
            return;
        }

        isExecuting = true;
    }

    //this is where the execution loop happens. Addition of commands must be done here.
    private void Update()
    {
        if (!isExecuting) return;

        if (!player.PathState()) return;

        if (player.IsCalculating()) return;

        string command = instruction_list.Dequeue();
        string param = null;

        if (set_of_context_keywords.Contains(command) || set_of_numeric_keywords.Contains(command))
        {
            param = instruction_list.Dequeue();
        }

        if (command == "step up") MoveDirection(1, Vector3.forward);
        if (command == "step down") MoveDirection(1, Vector3.back);
        if (command == "step left") MoveDirection(1, Vector3.left);
        if (command == "step right") MoveDirection(1, Vector3.right);

        if (command == "move up") MoveDirection(int.Parse(param), Vector3.forward);
        if (command == "move down") MoveDirection(int.Parse(param), Vector3.back);
        if (command == "move left") MoveDirection(int.Parse(param), Vector3.left);
        if (command == "move right") MoveDirection(int.Parse(param), Vector3.right);

        if (set_of_context_keywords.Contains(command)) GoTo(param);

        if (instruction_list.Count == 0)
        {
            isExecuting = false;
            input_controller.ResumeInput();
        }

    }

    private void GoTo(string param)
    {
        List<string> search_list = new List<string>(param.Split(' '));
        LexicalIdentity item = LexicalUniverseController.instance.FindObject(search_list);
        if (item != null) player.MoveTo(item.transform.position);
        else TextIO.instance.WriteToLog(Time.timeSinceLevelLoad + "; go-to object not found");
    }

    private void MoveDirection(int steps, Vector3 direction)
    {
        Vector3 init_pos = player.transform.position;
        Vector3 offset = new Vector3(0, 0, 0);
        for (int i = 0; i < steps; i++)
        {
            if (!AstarPath.active.data.gridGraph.GetNearest(init_pos + offset + direction).node.Walkable)
            {
                break;
            }
            offset += direction;
        }

        player.MoveTo(init_pos + offset);

    }

    public void Reset()
    {
        instruction_list.Clear();
        isExecuting = false;
        player.Reset();
    }

}
