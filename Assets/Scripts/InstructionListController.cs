﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionListController : MonoBehaviour {

    //Instruction list
    List<string> list_of_instructions = new List<string>();
    List<List<string>> list_history = new List<List<string>>();
    [SerializeField]
    ExecutionController executor;

    //the default function to expand the list
    public void Add_Command(string command)
    {
        list_history.Add(new List<string>(list_of_instructions));
        list_of_instructions.Add(command);
    }

    //a similar function, probably useful as a UI hook.
    public void Add_Context_Command(string command, string context)
    {
        list_history.Add(new List<string>(list_of_instructions));
        list_of_instructions.Add(command);
        list_of_instructions.Add(context);
    }

	public void Execute_Commands()
    {
        //send the list to the execution logic controller
        executor.BeginExecution(list_of_instructions);

        //clear the list for the next instruction iteration
        list_of_instructions.Clear();
        list_history.Clear();
    }

    public void Undo()
    {
        if (list_history.Count < 1) return;
        list_of_instructions = list_history[list_history.Count - 1];
        list_history.RemoveAt(list_history.Count - 1);
    }

    public List<string> readList() {
        return list_of_instructions;
    }
}
