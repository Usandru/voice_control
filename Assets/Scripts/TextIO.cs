﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextIO : MonoBehaviour {

    //variables
    public static TextIO instance;
    int log_id;
    string current_log_path;
    string config_dir = @"c:\VoiceControl";
    string config_path = @"c:\VoiceControl\config.txt";

	// Use this for initialization
	void Awake () {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        Directory.CreateDirectory(config_dir);

        if (!File.Exists(config_path))
        {
            StreamWriter config_creator = new StreamWriter(config_path);
            config_creator.WriteLine("1");
            log_id = 1;
            config_creator.Close();
        }
        else
        {
            StreamReader config_reader = new StreamReader(config_path);
            log_id = int.Parse(config_reader.ReadLine());
            config_reader.Close();
        }

        current_log_path = @"c:\VoiceControl\log_" + log_id + ".txt";
    }
	
    public void WriteToLog(string log_string)
    {
        StreamWriter log = new StreamWriter(current_log_path, true);
        log.WriteLine(log_string);
        log.Close();
    }

    public void NewLog()
    {
        EndLogging();
        current_log_path = @"c:\VoiceControl\log_" + log_id + ".txt";
    }

    public void EndLogging()
    {
        log_id++;
        File.WriteAllText(config_path, log_id.ToString());
    }

}
