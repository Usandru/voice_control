﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.Windows.Speech;
using System.Text;
using System.Linq;


public class KeyboardController: InputControllerABC {

	public InputField inputField;

	public InputField.SubmitEvent se;



	//Control commands
	string init_keyword = "instruction start";
	string exit_keyword = "instruction end";
//	string user_input = Console.ReadLine ();

	bool recievingInstructions = false;
	[SerializeField]
	InstructionListController instruction_list;
	[SerializeField]
	ExecutionController executor;





		//InputControllerABC.LogStringWithReturn(userInput);

	void InputComplete()
	{
		inputField.ActivateInputField();
		inputField.text = null;
	}
		
	void Start(){
		string userInput = inputField.text;
		se = new InputField.SubmitEvent ();
		inputField.onEndEdit = se;
		List<string> keyword_list = new List<string> { init_keyword, exit_keyword};
		keyword_list.AddRange(executor.GetCommands());
		keyword_list.AddRange(executor.GetContextCommands());





		//string userInput = inputField.text;

		if (recievingInstructions) {
			
			if (userInput == exit_keyword) 
			{
				instruction_list.Execute_Commands ();
			} 
			else 
			{
				if (executor.GetCommands ().Contains(userInput)) {
					InputComplete();
					if (executor.GetContextCommands().Contains(userInput)) 
						{
						print ("Success");
						} 
						else 
						{	
						print ("Need context");
						InputComplete();
						}
				} 
				else 
				{
					instruction_list.Add_Command(userInput);
				}

			}

		} 
		else 
		{
			if (userInput == init_keyword)recievingInstructions = true;
		}




	}
		
	



}


